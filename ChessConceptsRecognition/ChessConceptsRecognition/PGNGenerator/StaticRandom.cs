﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChessConceptsRecognition.PGNGenerator
{
    public static class StaticRandom
    {
        private static int _seed;
        public static Random Instance => ThreadLocal.Value;

        private static readonly ThreadLocal<Random> ThreadLocal = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref _seed)));
        static StaticRandom()
        {
            _seed = Environment.TickCount;
        }
    }
}
