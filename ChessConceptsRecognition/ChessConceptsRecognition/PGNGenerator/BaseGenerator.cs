﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Firefox;

namespace ChessConceptsRecognition.PGNGenerator
{
    public class BaseGenerator
    {
        public OpenQA.Selenium.Proxy Proxy { get; set; }

        protected FirefoxDriver InitFirefox(string url)
        {
            const int minPort = 7200;
            const int maxPort = 7800;
            var profile = new FirefoxProfile();
            
            profile.SetPreference("permissions.default.image", 2);
            profile.SetPreference("browser.cache.disk.enable", false);
            profile.SetPreference("browser.cache.offline.enable", false);


            profile.Port = StaticRandom.Instance.Next(minPort, maxPort);
            FirefoxDriver driver = null;
            try
            {
                driver = new FirefoxDriver(profile);
            }
            catch (Exception)
            {
                profile.Port = StaticRandom.Instance.Next(minPort, maxPort);
                driver?.Quit();
                driver = new FirefoxDriver(profile);
            }

            
            driver.Manage().Cookies.DeleteAllCookies();           
            driver.Navigate().GoToUrl(url);
            return driver;
        }
    }
}
