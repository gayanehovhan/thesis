﻿using Chess.Concepts.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessConceptsRecognition
{
    class Program
    {
        static void Main(string[] args)
        {
            PgnParser parser = new PgnParser();
            parser.Parse("resource/Kasparov.pgn");
        }
    }
}
