﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Concepts.Framework.Model.Game
{
    public class Board
    {
        private int[,] coords = new int[Constants.Board.Width, Constants.Board.Height];
        public int[,] coordinatesWithValues = new int[Constants.Board.Width, Constants.Board.Height];
        private List<IFigure> figures = new List<IFigure>();
        public List<IFigure> Figures
        {
            get
            {
                return this.figures;
            }
            set
            {
                this.figures = value;
            }
        }
        public int[,] CoordinatesWithValues
        {
            get
            {
                return this.coordinatesWithValues;
            }
            set
            {
                this.coordinatesWithValues = value;
            }
        }
        private IEnumerable<IFigure> GetInitialPawns(FigureColor color)
        {
            int y = color == FigureColor.White ? 1 : 6;
            int x = 0;
            for (int _x = x; _x < 8; _x++)
            {
                IFigure pawn = new Pawn(color);
                pawn.MoveTo(new Coord { X = _x, Y = y });
                CoordinatesWithValues[x, y] = color == FigureColor.White ? (int)FigureValue.WhitePawn : (int)FigureValue.BlackPawn;
                yield return pawn;
            }
        }

        private IEnumerable<IFigure> GetInitialRooks(FigureColor color)
        {
            int y = color == FigureColor.White ? 0 : 7;
            int x = 0;
            for (int _x = x; _x < 8; _x += 7)
            {
                IFigure figure = new Rook(color);
                figure.MoveTo(new Coord { X = x, Y = y });
                CoordinatesWithValues[x, y] = color == FigureColor.White ? (int)FigureValue.WhiteRook : (int)FigureValue.BlackRook;
                yield return figure;
            }
        }

        private IEnumerable<IFigure> GetInitialKnights(FigureColor color)
        {
            int y = color == FigureColor.White ? 0 : 7;
            int x = 1;
            for (int _x = x; _x < 8; _x += 5)
            {
                IFigure figure = new Knight(color);
                figure.MoveTo(new Coord { X = x, Y = y });
                CoordinatesWithValues[x, y] = color == FigureColor.White ? (int)FigureValue.WhiteKnigth : (int)FigureValue.BlackKnight;
                yield return figure;
            }
        }

        private IEnumerable<IFigure> GetInitialBishops(FigureColor color)
        {
            int y = color == FigureColor.White ? 0 : 7;
            int x = 2;
            for (int _x = x; _x < 8; _x += 3)
            {
                IFigure figure = new Bishop(color);
                figure.MoveTo(new Coord { X = x, Y = y });
                CoordinatesWithValues[x, y] = color == FigureColor.White ? (int)FigureValue.WhiteBishop : (int)FigureValue.BlackBishop;
                yield return figure;
            }
        }

        private IEnumerable<IFigure> GetInitialKings()
        {
            IFigure whiteKing = new King(FigureColor.White);
            IFigure blackKing = new King(FigureColor.Black);
            whiteKing.MoveTo(new Coord { X = 3, Y = 0 });
            blackKing.MoveTo(new Coord { X = 3, Y = 7 });
            CoordinatesWithValues[3, 0] = (int)FigureValue.WhiteKing;
            CoordinatesWithValues[3, 7] = (int)FigureValue.BlackKing;
            yield return whiteKing;
            yield return blackKing;
        }

        private IEnumerable<IFigure> GetInitialQueens()
        {
            IFigure whiteQueen = new Queen(FigureColor.White);
            IFigure blackQueen = new Queen(FigureColor.Black);
            whiteQueen.MoveTo(new Coord { X = 4, Y = 0 });
            blackQueen.MoveTo(new Coord { X = 4, Y = 7 });
            CoordinatesWithValues[4, 0] = (int)FigureValue.WHiteQueen;
            CoordinatesWithValues[4, 7] = (int)FigureValue.BlackQueen;
            yield return whiteQueen;
            yield return blackQueen;
        }
        private void Initialize()
        {
            IEnumerable<IFigure> whitePawns = GetInitialPawns(FigureColor.White);
            IEnumerable<IFigure> blackPawns = GetInitialPawns(FigureColor.Black);
            IEnumerable<IFigure> whiteRooks = GetInitialRooks(FigureColor.White);
            IEnumerable<IFigure> blackRooks = GetInitialRooks(FigureColor.Black);
            IEnumerable<IFigure> whiteKnights = GetInitialKnights(FigureColor.White);
            IEnumerable<IFigure> blackKnights = GetInitialKnights(FigureColor.Black);
            IEnumerable<IFigure> whiteBishops = GetInitialBishops(FigureColor.White);
            IEnumerable<IFigure> blackBishops = GetInitialBishops(FigureColor.Black);
            IEnumerable<IFigure> kings = GetInitialKings();
            IEnumerable<IFigure> queens = GetInitialQueens();
            this.figures.AddRange(whitePawns);
            this.figures.AddRange(blackPawns);
            this.figures.AddRange(whiteRooks);
            this.figures.AddRange(blackRooks);
            this.figures.AddRange(whiteKnights);
            this.figures.AddRange(blackKnights);
            this.figures.AddRange(whiteBishops);
            this.figures.AddRange(blackBishops);
            this.figures.AddRange(kings);
            this.figures.AddRange(queens);
            for (var i = 0; i <= 7; i++)
            {
                for (var j = 0; j <= 7; j++)
                {
                    if (CoordinatesWithValues[i, j] != 0)
                    {
                        CoordinatesWithValues[i, j] = 0;
                    }
                }
            }
        }
        public Board()
        {
            this.Initialize();
        }
        public int[,] GetBoardForMoveNumber(string pgnFilePath, int moveNumber)
        {
            return new int[,] { };
        }
    }
}
