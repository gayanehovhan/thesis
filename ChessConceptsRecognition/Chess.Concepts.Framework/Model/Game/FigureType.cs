﻿namespace Chess.Concepts.Framework.Model.Game
{
    public enum FigureColor
    {
        Black,
        White
    }
    public enum  FigureValue
    {
        Empty =  0,
        WhitePawn = 1,
        BlackPawn = -1,
        WhiteRook = 2,
        BlackRook = -2,
        WhiteKnigth = 3,
        BlackKnight = -3,
        WhiteBishop = 4,
        BlackBishop = -4,
        WHiteQueen = 5,
        BlackQueen = -5,
        WhiteKing = 6,
        BlackKing = -6
    }
}