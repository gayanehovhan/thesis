﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Concepts.Framework.Model.Game
{
    public interface IFigure
    {
        bool IsValideMove(Coord coordinate);
        bool IsCaptured { get; set; }
        Coord Position { get; set; } 
        void MoveTo(Coord position);
    }
}
