﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Concepts.Framework.Model.Game
{
    public abstract class FigureBase : IFigure
    {
        protected Coord position;
        protected FigureColor figureColor;
        protected bool isCaptured;
        public bool IsCaptured
        {
            get
            {
                return this.isCaptured;
            }
            set
            {
                this.isCaptured = value;
            }
        }
        public Coord Position
        {
            get
            {
                return this.position;
            }
            set
            {
                this.position = value;
            }
        }



        public void MoveTo(Coord position)
        {
            this.position = position;
        }
        public abstract bool IsValideMove(Coord coordinate);
        public FigureBase(FigureColor figureColor)
        {
            this.figureColor = figureColor;
        }
    }
}
