﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Concepts.Framework.Model.Game
{
    public class Pawn : FigureBase
    {
        public Pawn(FigureColor figureColor) : base(figureColor)
        {
        }

        public bool IsFirstMove
        {
            get
            {
                return this.figureColor == FigureColor.White && this.position.Y == 1 ||
                       this.figureColor == FigureColor.Black && this.position.Y == 6;

            }

        }
      
        public override bool IsValideMove(Coord coorinates)
        {
            throw new NotImplementedException();
        }
    }
}
