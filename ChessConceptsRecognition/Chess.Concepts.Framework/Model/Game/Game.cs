﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Concepts.Framework.Model.Game
{
    public class ChessGame
    {
        private Board board;
        private ChessGame(Board board)
        {
            this.board = board;
        }
        //"resource/Kasparov.pgn"
        public static ChessGame LoadFromPgn(string pgnFilePath)
        {
            var board = new Board();
            PgnParser parser = new PgnParser();
            var games = parser.Parse(pgnFilePath);
            var game = games?.FirstOrDefault();
            var moveText = game.MoveText;
            if(moveText != null)
            {
                foreach(var move in moveText.GetMoves())
                {
                    var coord = Utilities.ParseMove(move.ToString(), ref board);                    
                }
            }
            return new ChessGame(board);
        }
    }
}
