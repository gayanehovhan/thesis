﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Concepts.Framework
{
    public static class Constants
    {
        public static class Board
        {
            public const int Width = 8;
            public const int Height = 8;
        }
    }
}
