﻿using Chess.Concepts.Framework.Model.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Concepts.Framework
{
    public static class Utilities
    {
        private static int ConvertCoordToY(string coord)
        {
            int coordVal = int.Parse(coord);
            return coordVal - 1;
        }
        private static int ConvertCoordToX(char coord)
        {
            int coordVal = 0;
            switch (coord)
            {
                case 'a':
                    {
                        coordVal = 0;
                    }
                    break;
                case 'b':
                    {
                        coordVal = 1;
                    }
                    break;
                case 'c':
                    {
                        coordVal = 2;
                    }
                    break;
                case 'd':
                    {
                        coordVal = 3;
                    }
                    break;
                case 'e':
                    {
                        coordVal = 4;
                    }
                    break;
                case 'f':
                    {
                        coordVal = 5;
                    }
                    break;
                case 'g':
                    {
                        coordVal = 6;
                    }
                    break;
                case 'h':
                    {
                        coordVal = 7;
                    }
                    break;
            }
            return coordVal;
        }
        public static Type ParseFigureType(char pgnType)
        {
            switch (pgnType)
            {
                case 'Q':
                    {
                        return typeof(Queen);
                    };

                case 'N':
                    {
                        return typeof(Knight);
                    };
                case 'B':
                    {
                        return typeof(Bishop);
                    };
                case 'R':
                    {
                        return typeof(Rook);
                    };
                case 'K':
                    {
                        return typeof(King);
                    };
                default:
                    {
                        return typeof(Pawn);
                    }
            }
        }



        public static Coord ParseMove(string pgnCoord, ref Board board)
        {
            int x, y;
            int i = 0;
            var figureType = pgnCoord[i];
            var type = ParseFigureType(figureType);
            var xlocationOfPawn = 0;
            i++;
            var file = pgnCoord[i];
            char rank = ' ';
            bool isCaptured = file == 'x';
            i++;
            if (isCaptured)
            {
                file = pgnCoord[i];
            }
            if (type == typeof(Pawn))
            {
                x = xlocationOfPawn = ConvertCoordToX(figureType);
                y = ConvertCoordToY(file.ToString());
            }
            else
            {
                i++;
                rank = pgnCoord[i];
                x = ConvertCoordToX(file);
                y = ConvertCoordToY(rank.ToString());
            }
            var coord = new Coord(x, y);
            var pawns = board.Figures.Where(f => f.GetType() == type);
            if (pawns != null)
            {
                foreach (var figure in pawns)
                {
                    if (IsPawnValid(figure, coord, isCaptured))
                    {
                        continue;
                    }
                    //figure.Position = coord;
                    //return coord;
                }

            }            throw new Exception("pgn file is invalid");
        }

        private static  bool IsPawnValid(IFigure figure,Coord coordinate,bool isCaptured)
        {
            var pawn = figure as Pawn;
            bool isValid = false;
            if(pawn != null)
            {
                var pawnCoord = pawn.Position;
                if(pawnCoord != null && coordinate != null)
                {
                    var x = pawnCoord.X;
                    var y = pawnCoord.Y;
                    if(pawn.IsFirstMove)
                    {
                        var diffY = coordinate.Y - y;
                        var diffX = Math.Abs(coordinate.X - x);
                        if(!isCaptured && (diffY == 2 || diffY == 1) && diffX == 0)
                        {
                            isValid = true;
                        }
                        if(isCaptured && diffX == 1 && diffY == 1)
                        {
                            isValid = true;
                        }
                    }
                }
            }
            return isValid;
        }
    }
}
