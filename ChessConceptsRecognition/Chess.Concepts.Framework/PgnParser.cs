﻿using ilf.pgn;
using ilf.pgn.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Concepts.Framework
{
    public class PgnParser
    {
        /// <summary>
        /// parses pgn file then converts it to Game objects sequance
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public IEnumerable<Game> Parse(string path)
        {
            var reader = new PgnReader();
            var games = reader.ReadFromFile(path);
            return games.Games;
        }
    }
}
