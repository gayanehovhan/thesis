﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ChessConceptsRecognition.PGNGenerator;

namespace Test
{
    [TestClass]
    public class ChessGameGeneratorTest
    {
        [TestMethod]
        public void DownloadChessGame()
        {
            var chessGameGenerator = new ChessGameGenerator();
            chessGameGenerator.DownloadChessGames();
        }
    }
}
