﻿using Chess.Concepts.Framework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    [TestClass]
    public class Pgn
    {
        static private readonly string pgnFilePath = @"pgn-files/Kasparov.pgn";
        [TestMethod]
        public void ReadPgnAndCheckCountOfGames()
        {
            var parser = new PgnParser();
            var games = parser.Parse(pgnFilePath);
            Assert.IsNotNull(games, "Returned games from parser is null");
            var count  = games.Count();
            Assert.IsTrue(count > 0, "There is no game in given pgn file ");
        }

    }
}
