export class Game {
  public Id: number
  public WhitePlayer:string
  public BlackPlayer: string
  public Date: string
  public Result: string
  public ECO: string
}

export class ResultData {
  public Data: Game[];
}
export class Players {
  public Data:Player[]
}
export class Player {
  public Name: string
}
