import { Role, Color, Key } from "chessground/types";
import { when } from "q";

export class ChessConcept {
  public Fen: string;
  public Pawns: Pawn[];
  public ChessConcept() {
    this.Fen = '';
    this.Pawns = [];
  }
}
export class FenString {
  public Fen: string;
  constructor(f) {
    this.Fen = f;
  }

}
export class Pawn {
  public Row: number;
  public Col: number;
  public IsTransient: boolean;
}
export enum BoardPosition {
  a,
  b,
  c,
  d,
  e,
  f,
  g,
  h
}
export class Pieces {
  public Pawn: Role = 'pawn'
  public King: Role = 'king'
  public Queen: Role = 'queen'
  public Rook: Role = 'rook'
  public Bishop: Role = 'bishop'
  public Knight: Role = 'knight'

}
export class PieceColor {
  public White: Color = 'white'
  public Black: Color = 'black'
}
export class PawnConcepts {
  public static readonly IsTransient: string = "Զինվորը անցողիկ է։";
}
export class MateOnKing {
  public static readonly IsMateOnKing: string = "Առկա է մատային գրոհ թագավորի վրա։";
}
export class DirectMateOnKing {
  public static readonly IsDirectMateOnKing: string = "Առկա է ուղիղ գրոհ թագավորի վրա։";
}
export class TransientPawnDict {
  public Position: Key
  public ConceptLV: string
  public TransientColor: TColor

  constructor(pos: Key, lv: string, tc: string) {
    this.Position = pos;
    this.ConceptLV = lv;
    this.TransientColor = tc;
  }
}
export class TColor {
  public static readonly MinC: string = '#fff'
  public static readonly MidC: string = '#fff'
  public static readonly MaxC: string = '#fff'
}

