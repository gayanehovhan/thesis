import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CoreModule } from './main/core/core.module'
import { AppComponent } from './app.component';
import { BaseModule } from '@base/base.module';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CoreModule.forRoot(),
    BaseModule.forRoot(),
    HttpClientModule,
    CommonModule,
    RouterModule.forRoot([])
  ],
  providers: [ ],
  bootstrap: [AppComponent]
})
export class AppModule { }
