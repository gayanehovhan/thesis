import { ApiService } from './api.service';
import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';

@NgModule({
    imports: [],
    declarations: [ ],
    exports: [ ],
    providers: [ ApiService ]
})
export class CoreModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule,
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
        if (parentModule) {
            throw new Error(
                'CoreModule is already loaded. Import it once only');
        }
    }

 }
