import { EnvironmentService } from '@base/environment.service';
import { Injectable } from '@angular/core';

import { HttpIntercept, Response, IRequestOptions } from '@base/httpintercept.service';
import { ResultData, Players } from "models/gameInfo";
import { Observable } from 'rxjs';
import { ChessConcept, FenString } from 'models/moveInfo';
import { Color } from 'chessground/types';

@Injectable()
export class ApiService {
  constructor(private http: HttpIntercept, private env: EnvironmentService) { }

  getChessGames(): Observable<ResultData> {
    return this.http.get<ResultData>(this.env.apiUrl + 'concepts/games');
  }
  getChessGamesByFilter(playerName: string, color: Color, playerDate: string, oponentName: string): Observable<ResultData> {
    return this.http.get<ResultData>(this.env.apiUrl + 'concepts/gamebyfilter?playerName=' + playerName + '&color=' + color + '&playerDate=' + playerDate + '&oponentName=' + oponentName)
  }

  getPlayers(): Observable<Players> {
    return this.http.get<Players>(this.env.apiUrl + 'concepts/players');
  }
  getChessGameMoveFen(gameId: number, moveId: number): Observable<ChessConcept> {
    return this.http.get<ChessConcept>(this.env.apiUrl + 'concepts/move/' + gameId + '/' + moveId);
  }
  getMovesCount(gameId: number): Observable<number> {
    return this.http.get<number>(this.env.apiUrl + 'concepts/move/movescount/' + gameId);
  }
  getConceptsByFen(fen: string): Observable<ChessConcept> {
    return this.http.get<ChessConcept>(this.env.apiUrl + 'concepts/move/byfen?fen=' + fen);
  }
  getMateOnKingCoeff(fen: string): Observable<number> {
    return this.http.get<number>(this.env.apiUrl + 'concepts/move/mateonking?fen=' + fen);
  }
  getDirectMateOnKingCoeff(fen: string): Observable<number> {
    return this.http.get<number>(this.env.apiUrl + 'concepts/move/directmateonking?fen=' + fen);
  }
}
