import { Component, OnInit, OnDestroy } from '@angular/core';
import { Api, start } from 'chessground/api';
import { Chessground } from 'chessground';
import { Piece, Role, Key, Color } from 'chessground/types'
import { pos2key, key2pos, opposite, containsX } from 'chessground/util'
import { Game, ResultData, Player, Players } from 'models/gameInfo';
import { ApiService } from './main/core/api.service';
import { Subscription, interval, BehaviorSubject, Observable, timer } from 'rxjs';
import { DebugHelper } from 'protractor/built/debugger';
import { ChessConcept, BoardPosition, Pieces, PieceColor, PawnConcepts, TransientPawnDict, Pawn, MateOnKing, DirectMateOnKing, FenString } from 'models/moveInfo';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';
import { forEach } from '@angular/router/src/utils/collection';
import { callbackify } from 'util';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  player1Name: string;
  selectedColor: Color;
  gameDate: number;
  player2Name: string;

  year: number;
  dateRange: number[] = [];

  cg: Api
  newcg: Api;
  subscription: Subscription;
  games: ResultData;
  gameName: string;
  isNewBoard: boolean = false;
  moveinfo: ChessConcept;
  currentGameId: number;
  currentMoveId: number = 0;
  movescount: number;
  moveNumber: number = 1;
  mateOnKing: string;
  directMateonking: string;
  players: Players;

  pieces: Pieces = new Pieces();
  color: PieceColor = new PieceColor()

  transientPawns: TransientPawnDict[] = [];

  constructor(private api: ApiService) {
  }

  ngOnInit() {

    this.year = new Date().getFullYear();
    for (var i = 112; i >= 0; i--) {
      this.dateRange.push(this.year - i);
    }
  }

  getGames(player1Name: string, selectedColor: Color, date: number, player2Name: string) {
    this.subscription = this.api.getChessGamesByFilter(player1Name, selectedColor, date != null ? date.toString() : "0", player2Name)
      .subscribe(n => {
        this.games = n;
      });
  }

  clearFilter() {
    this.player1Name = null;
    this.player2Name = null;
    this.selectedColor = null;
    this.gameDate = null;
    this.games =null;
  }
  loadPlayers() {
    this.clearFilter();
    this.subscription = this.api.getPlayers()
      .subscribe(n => {
        this.players = n;
      });
  }

  showTransientPawns(pawns: Pawn[]) {
    this.transientPawns = [];
    pawns.forEach(p => {
      if (p.IsTransient)
        this.transientPawns.push(new TransientPawnDict(pos2key([p.Col + 1, p.Row]), PawnConcepts.IsTransient, '#fff'));
    })
  }

  selectGame(game: Game) {

    document.getElementById('emptyboardwrapper').style.display = 'none';
    this.gameName = 'Սպիտակ - ' + game.WhitePlayer + ' | Սև - ' + game.BlackPlayer + ' | ' + this.getNormalDate(game.Date) + ' | Հաշիվը ' + game.Result + ' | Սկզբնաղաղը ' + game.ECO;
    this.isNewBoard = false;
    this.currentGameId = game.Id;

    this.api.getMovesCount(this.currentGameId).subscribe(c => this.movescount = c);
    this.subscription = this.api.getChessGameMoveFen(game.Id, 0)
      .subscribe(n => {
        this.moveinfo = n;
        this.cg = Chessground(document.getElementById('dirty'), {
          animation: {
            duration: 500
          },
          fen: this.moveinfo.Fen,
          movable: {
            free: false
          }
        });
        this.showTransientPawns(n.Pawns);
      });
  }
  onPositionClick(pos: Key) {
    this.cg.selectSquare(pos)
  }
  onPositionClickNewGame(pos: Key) {
    this.newcg.selectSquare(pos)
  }
  getGameName(game: Game): string {
    return game.WhitePlayer + ' | ' + game.BlackPlayer + ' | ' + this.getNormalDate(game.Date) + ' | ' + game.ECO;
  }

  getNormalDate(date: string): string {
    if (date.indexOf('?') > -1) {
      return date.slice(0, date.indexOf('?') - 1);
    }
    return date;
  }

  refreshBoardState() {
    this.moveNumber = 1;
    this.subscription = this.api.getChessGameMoveFen(this.currentGameId, 0)
      .subscribe(n => {
        this.moveinfo = n;
        this.cg.set({
          animation: {
            duration: 500
          },
          fen: this.moveinfo.Fen,
        })
        this.showTransientPawns(n.Pawns);
      });
  }
  playGame() {
    const source = interval(1000);
    this.subscription = this.api.getMovesCount(this.currentGameId)
      .subscribe(n => {
        source
          .takeUntil(timer(n * 1000 + 1000))
          .switchMap(n => this.api.getChessGameMoveFen(this.currentGameId, this.currentMoveId++))
          .subscribe(n => {
            this.moveinfo = n;
            this.cg.set({
              animation: {
                duration: 500
              },
              fen: this.moveinfo.Fen,
            });
            this.showTransientPawns(n.Pawns);
          })
      });
  }
  moveBackward() {
    if (this.moveNumber >= 0) {
      this.subscription = this.api.getChessGameMoveFen(this.currentGameId, this.moveNumber--)
        .subscribe(n => {
          this.moveinfo = n;
          this.cg.set({
            animation: {
              duration: 500
            },
            fen: this.moveinfo.Fen,
          });
          this.showTransientPawns(n.Pawns);
        });
    }
  }
  moveForward() {
    if (this.moveNumber <= this.movescount) {
      this.subscription = this.api.getChessGameMoveFen(this.currentGameId, this.moveNumber++)
        .subscribe(n => {
          this.moveinfo = n;
          this.cg.set({
            animation: {
              duration: 500
            },
            fen: this.moveinfo.Fen,
          });
          this.showTransientPawns(n.Pawns);
        });
    }
  }

  setNewGame() {
    this.transientPawns = [];
    this.isNewBoard = true;
    document.getElementById('emptyboardwrapper').style.display = 'block';
    this.newcg = Chessground(document.getElementById('emptyBoard'));
  }

  setPiece(piece: Role, pieceColor: Color) {

    const np: Piece = { role: piece, color: pieceColor };

    this.newcg.newPiece(np, 'a1')

    this.newcg.set({
      movable: {
        free: true
      }
    });
  }

  showInitialBoard() {
    this.newcg.set({
      animation: {
        duration: 500
      },
      fen: 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR',
      movable: {
        free: true
      }
    })
  }

  getTransientPawnByFen() {
    this.subscription = this.api.getConceptsByFen(this.newcg.getFen() + " w - - 0 1")
      .subscribe(n => {
        this.showTransientPawns(n.Pawns)
      });
  }
  IsMateOnKing(coeff: number): string {
    return coeff > 0.8 ? MateOnKing.IsMateOnKing : null;
  }
  IsDirectMateOnKing(coeff: number): string {
    return coeff > 0.8 ? DirectMateOnKing.IsDirectMateOnKing : null;
  }
  getMateOnKingConcept() {

    this.subscription = this.api.getMateOnKingCoeff(this.newcg.getFen() + " b - - 0 1")
      .subscribe(n => {
        this.mateOnKing = this.IsMateOnKing(n);
      });
  }

  getDirectMateOnKingConcept() {

    this.subscription = this.api.getDirectMateOnKingCoeff(this.newcg.getFen() + " b - - 0 1")
      .subscribe(n => {
        this.directMateonking = this.IsDirectMateOnKing(n);
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}

