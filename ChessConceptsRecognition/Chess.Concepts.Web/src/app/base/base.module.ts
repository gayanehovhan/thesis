import { Location } from '@angular/common';
import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { RequestOptions, XHRBackend } from '@angular/http';


import { ConfigService } from './config.service';
import { EnvironmentService } from './environment.service';
import { HttpIntercept } from './httpintercept.service';
import { UserAuthenticated } from './authguard.service';
import { HttpClient } from '@angular/common/http';

export function HttpInterceptFactory(client: HttpClient, options: RequestOptions, env: EnvironmentService, loc: Location) {
        return new HttpIntercept(client, loc, env);
}

export function returnFalse() {
    return false;
}


@NgModule({
    imports: [ ],
    declarations: [ ],
    exports: [ ],
    providers: [ EnvironmentService, ConfigService, UserAuthenticated,
    {
      provide: HttpIntercept,
      useFactory: HttpInterceptFactory,
      deps: [HttpClient, EnvironmentService, Location]
    },
    {
        provide: 'NeverActivateGuard',
            useValue: returnFalse
    }
    ]
})
export class BaseModule {

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: BaseModule
        };
    }


    constructor( @Optional() @SkipSelf() parentModule: BaseModule) {
        if (parentModule) {
            throw new Error(
                'CoreModule is already loaded. Import it in the AppModule only');
        }
    }


 }
