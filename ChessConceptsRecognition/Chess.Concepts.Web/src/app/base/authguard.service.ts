import { Observable } from 'rxjs/Observable';
import { ConfigService } from './config.service';
import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanLoad } from '@angular/router';
//import { User } from "models/user";

@Injectable()
export class UserAuthenticated implements CanActivate, CanActivateChild, CanLoad {
    user: any;
    constructor(private config: ConfigService) {}

    canActivate(): Observable<boolean> {
        return this.userKnown();
    }

    canActivateChild(): Observable<boolean>  {
        return this.userKnown();
    }

    canLoad(): Observable<boolean>  {
        return this.userKnown();
    }

    private userKnown(): Observable<boolean>  {
        return this.config.user.asObservable()
            //.map((user: User) => true);
    }

}
