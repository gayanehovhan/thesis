import { ResponseOptions } from '@angular/http';
import { EnvironmentService } from './environment.service';
import { Injectable } from '@angular/core';
import { HttpIntercept, Headers, Response } from './httpintercept.service';

import { AsyncSubject, Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
//import { User } from "models/user";

declare var ENVVAR_BRANDURL;
declare var ENVVAR_CDNURL;
declare var ENVVAR_APIURL;

@Injectable()
export class ConfigService {

  private apiUrl: string;
  private brandUrl: string;
  private cdnUrl: string;

  public user:any //AsyncSubject<User>;
  public appSwitcherScript: AsyncSubject<String>;

  public jsonHeaders: Headers;

  constructor(private http: HttpIntercept, private env: EnvironmentService) {

    this.apiUrl = env.apiUrl;
  }

  //  this.user = new AsyncSubject<User>();
  //  //this.appSwitcherScript = new AsyncSubject<String>();

  //  this.GetUser()
  //    .then((user: User) => {
  //      this.user.next(user);
  //      this.user.complete();
  //      //this.GetAppSwitcherScript(user.UserId)
  //      //  .then((script: string) => {
  //      //    this.appSwitcherScript.next(script);
  //      //    this.appSwitcherScript.complete();
  //      //  });
  //    });
  //}

  //private GetUser(): Promise<User> {
  //    return this.http.get<User>(this.apiUrl + 'User')
  //        .toPromise()
  //        .then((user:User) => user);
  //}

  //private GetAppSwitcherScript(userId: number): Promise<string> {

  //    return this.http.get<string>(this.brandUrl + 'cdn/switcher/' + userId.toString() + '/appSwitcher.js', { withCredentials: true })
  //      .catch((err, source) => {
  //          if (this.env.environment === 'DEVELOPMENT' && (err.status  === 0) ) {
  //              console.log('App Switcher cannot be retrieved: has this been disallowed by CORS?');
  //              return Observable.of(new Response(new ResponseOptions({body: ''})));
  //          } else {
  //              return Observable.throw(err);
  //          }
  //      })
  //      .toPromise()
  //      .then((response: Response) => response.text() as string);
  //}

}
