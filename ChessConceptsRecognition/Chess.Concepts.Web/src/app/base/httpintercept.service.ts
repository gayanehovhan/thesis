import { EnvironmentService } from './environment.service';
import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders, HttpParams} from '@angular/common/http'
export { Headers, Request, Response} from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Location } from '@angular/common';
import 'rxjs/add/operator/catch'

@Injectable()
export class HttpIntercept  {

    private apiUrl: string;

    constructor (private http: HttpClient, private loc: Location, env: EnvironmentService) {
        this.apiUrl = env.apiUrl;
    }

    get<T>(url: string, options?: IRequestOptions): Observable<T> {
        return this.intercept(this.http.get<T>(url, options))
    }
    post<T>(url: string, body: any, options?: IRequestOptions): Observable<T> {
        return this.intercept(this.http.post<T>(url, body, options))
    }

    //This is a workaround until this angular issue is resolved: https://github.com/angular/angular/issues/18680
    postNoResponse(url: string, body: any, options?: ITextRequestOptions | IRequestOptions): Observable<{}> {
      return this.intercept(this.http.post(url, body, { responseType: 'text' }));
    }
    
    put<T>(url: string, body: any, options: IRequestOptions): Observable<T> {
        return this.intercept(this.http.put<T>(url, body, options))
    }
    delete<T>(url: string, options: IRequestOptions): Observable<T> {
        return this.intercept(this.http.delete<T>(url, options))
    }
   
  intercept<T>(observable: Observable<T>): Observable<T> {
        return observable.catch((err: Response, source) => {
            if (err.status  === 401) {
                if (this.loc.normalize(err.url.toLowerCase()).includes(this.loc.normalize(this.apiUrl.toLowerCase()))) {
                    window.location.href = '/';
                    return Observable.empty();
                }
            }
            return Observable.throw(err);
        });
    }

}

export  interface IRequestOptions {
    headers?: HttpHeaders,
    observe?: 'body',
    params?: HttpParams,
    reportProgress?: boolean,
    responseType?: 'json',
    withCredentials?: boolean
}

export interface ITextRequestOptions {
  headers?: HttpHeaders,
  observe?: 'body',
  params?: HttpParams,
  reportProgress?: boolean,
  responseType?: 'text',
  withCredentials?: boolean
}
