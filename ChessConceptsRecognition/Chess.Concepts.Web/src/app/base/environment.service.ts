import { Injectable } from '@angular/core';

declare var ENVVAR_BRANDURL;
declare var ENVVAR_CDNURL;
declare var ENVVAR_APIURL;
declare var ENVVAR_ENVIRONMENT;
declare var ENVVAR_RELEASENUMBER;


@Injectable()
export class EnvironmentService {

  public apiUrl: string;
  public environment: string = 'UNDEFINED';
  public releaseNumber: string = 'UNDEFINED';
  

  constructor() {

    // Environmental variables defined onto global scope
    // in index.html

    this.apiUrl = ENVVAR_APIURL;

    this.environment = ENVVAR_ENVIRONMENT;
    this.releaseNumber = ENVVAR_RELEASENUMBER;

  }

  private checkUrl(url: string) {
    if (!url.endsWith('/')) {
      return url + '/';
    }
    return url;
  }


}
