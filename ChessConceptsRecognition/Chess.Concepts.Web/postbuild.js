﻿var fs = require('fs');

FILEPATH = 'dist/index.html';
DBREGEXP = new RegExp(
  /<\!--DEVBUILD-->[\S\s]*<\!--\/DEVBUILD-->/
  , 'g');
OB1REGEXP = new RegExp(
  /<\!--OCTOBUILD>/
  , 'g');
OB2REGEXP = new RegExp(
  /<\!--OCTOBUILD>/
  , 'g');

console.log("Preparing Octopus Variable Replacement Strings");
fs.readFile(FILEPATH, 'utf8', function (err, data) {
  if (err) {
    return console.log(err);
  }

  data = data.replace(DBREGEXP, '');
  data = data.replace(OB1REGEXP, '');
  data = data.replace(OB2REGEXP, '');

  fs.writeFile(FILEPATH, data, 'utf8', function (err) {
    if (err) return console.log(err);
  });
  console.log("Octopus Variable Replacements Complete");
});
