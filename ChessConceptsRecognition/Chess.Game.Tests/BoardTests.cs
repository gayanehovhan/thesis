﻿using Chess.Concepts.Framework;
using Chess.Concepts.Framework.Model.Game;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Game.Tests
{
    [TestClass]
    public class BoardTests
    {
        [TestMethod]
        public void ChessBoardAfterInitializationShouldHave16Pawns()
        {
            Board board = new Board();
            var figures = board.Figures;
            var pawns = figures.Where(p => p.GetType() == typeof(Pawn)).ToList();
            var count = pawns.Count;
            Assert.AreEqual(16, count);
        }

        [TestMethod]
        public void ChessBoardAfterInitializationShouldHave32Figures()
        {
            Board board = new Board();
            var count = board.Figures.Count;
            Assert.AreEqual(32, count);
        }

        [TestMethod]
        public void TestPgnMoveConvertion()
        {
            var move = "Qxd4";
            var board = new Board();
            Utilities.ParseMove(move, ref board);
        }

        [TestMethod]
        public void PgnLoadCheck()
        {
            var game = ChessGame.LoadFromPgn(@"resource/Kasparov.pgn");
        }

        [TestMethod]
        public void BoardStateTest()
        {
            var board = new Board();
            var game = ChessGame.LoadFromPgn(@"resource/Kasparov.pgn");
            
            int[,] boardValues = board.GetBoardForMoveNumber(@"resource/Kasparov.pgn", 10);

        }
    } 
}
