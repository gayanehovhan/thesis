﻿using ChessDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Main
{
    public static class Extensions
    {
        public static bool IsWhite(this Piece figure)
        {
            return figure != null && figure.Owner.ToString() == Constants.Figures.WhiteColorName;
        }
    }
}
