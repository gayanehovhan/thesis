﻿using ChessDotNet;
using ChessDotNet.Pieces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Main
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("test");
            var allPawns = new List<ChessFigure>();
            var fens = FenRepository.GetFenByGameIdAndMove(1, 1);
            var game = new ChessGame("rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1");
            Piece[][] board = game.GetBoard();

            for (int i = 7; i >= 0; i--)
            {
                for (int j = 0; j < 8; j++)
                {
                    var figure = board[i][j];
                    var desc = "empty";
                    if (figure != null && figure.GetType() == typeof(Pawn))
                    {
                        desc = board[i][j].GetType().ToString().Replace("ChessDotNet.Pieces", "");
                        var chessFigure = new ChessFigure
                        {
                            figure = figure,
                            Col = j,
                            Row = i
                        };
                        allPawns.Add(chessFigure);
                    }
                    Console.Write("[" + i + "," + j + "]" + " | ");
                }
                Console.WriteLine();
            }

            Console.WriteLine(game.GetBoard());
            foreach (var pawn in allPawns)
            {
                var coeff = ChessConcepts.GetTransientPawnCoeff(pawn);
                var lv = ChessConcepts.GetPawnTransientLV(coeff);
                Console.WriteLine($"{pawn.Row} | {pawn.Col} | {coeff} | {lv}");
            }
            //        var games = FenRepository.GetGames();
            Console.ReadLine();
        }
    }
}
