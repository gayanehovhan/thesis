﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Main
{
    public static class Constants
    {
        public static class Figures
        {
            public static readonly string WhiteColorName = "White";
        }
        public static class Concepts
        {
            public static class PawnConcepts
            {
                public static readonly string MinTransient = "The pawn have minimum transient degree";
                public static readonly string MidTransient = "The pawn have medium transient degree";
                public static readonly string MaxTransient = "The pawn have maximum transient degree";
            }
        }
    }
}
