﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Main
{
    public class PawnDTO
    {
        public int Row { get; set; }
        public int Col { get; set; }
        public bool IsTransient { get; set; }
        
    }
    public class ChessConceptDTO
    {
        public static ChessConceptDTO Empty
        {
            get
            {
                return new ChessConceptDTO
                {
                    Fen = string.Empty
                };
            }
        }


        public string Fen { get; set; }
        public List<PawnDTO> Pawns { get; set; } = new List<PawnDTO>();
    }
    public class Fen
    {
        public string FenString { get; set; }
    }
}
