﻿using ChessDotNet;
using ChessDotNet.Pieces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Chess.Main.Constants.Concepts;

namespace Chess.Main
{
    public static class ChessConcepts
    {

        private static Dictionary<int, File> fileMapping = new Dictionary<int, File>
        {
            [0] = File.A,
            [1] = File.B,
            [2] = File.C,
            [3] = File.D,
            [4] = File.E,
            [5] = File.F,
            [6] = File.G,
            [7] = File.H,
        };

        private static Dictionary<int, int> rankMapping = new Dictionary<int, int>
        {
            [0] = 1,
            [1] = 2,
            [2] = 3,
            [3] = 4,
            [4] = 5,
            [5] = 6,
            [6] = 7,
            [7] = 8,
        };

        public static ChessConceptDTO GetConcepts(string fen)
        {
            Console.WriteLine("test");
            var allPawns = new List<ChessFigure>();
            var fens = FenRepository.GetFenByGameIdAndMove(1, 1);
            var game = new ChessGame(fen);
            Piece[][] board = game.GetBoard();

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    var figure = board[i][j];
                    var desc = "empty";
                    if (figure != null && figure.GetType() == typeof(Pawn))
                    {
                        desc = board[i][j].GetType().ToString().Replace("ChessDotNet.Pieces", "");

                        var file = fileMapping[j];
                        var rank = rankMapping[7 - i];
                        var position = new Position { File = file, Rank = rank };
                        var validMoveCount = 0;
                        validMoveCount = game.GetValidMoves2(position,false).Count();
                 
                        var chessFigure = new ChessFigure
                        {
                            figure = figure,
                            Col = j,
                            Row = i,
                            ValidMovesCount = validMoveCount
                        };
                        allPawns.Add(chessFigure);
                    }
                    Console.Write(desc + " | ");
                }
                Console.WriteLine();

            }
            var pawnDTOs = new List<PawnDTO>();
            foreach (var pawn in allPawns)
            {
                //var coeff = GetTransientPawnCoeff(pawn);
                //var lv = GetPawnTransientLV(coeff);
                var isTransient = IsTransientPawn(pawn, allPawns);
                var row = pawn.Row;
                if (!pawn.IsWhite) { row = 8 - pawn.Row; }
                var pawnDTO = new PawnDTO
                {
                    Row = 8 - pawn.Row,
                    Col = pawn.Col,
                    IsTransient = isTransient,
                };
                pawnDTOs.Add(pawnDTO);
            }
            var conceptDTO = new ChessConceptDTO
            {
                Fen = fen,
                Pawns = pawnDTOs,
            };
            return conceptDTO;

        }
        public static string GetPawnTransientLV(double coefficent)
        {
            if (coefficent >= 0 && coefficent < 0.3)
            {
                return PawnConcepts.MinTransient;
            }
            else if (coefficent >= 0.3 && coefficent < 0.7)
            {
                return PawnConcepts.MidTransient;
            }
            return PawnConcepts.MaxTransient;
        }
        public static double GetTransientPawnCoeff(ChessFigure figure)
        {
            var row = figure.Row;

            if (figure.IsWhite)
            {
                return (6.0 - row) / 6.0;
            }
            else
            {
                return row / 7.0;

            }
        }
        public static bool IsTransientPawn(ChessFigure pawn, List<ChessFigure> allPawns)
        {
           return  GetPawnTransientLV(GetTransientPawnCoeff(pawn)) == PawnConcepts.MaxTransient
                && pawn.ValidMovesCount > 0;
        }
        public static bool IsPawnOnVerticalLine(List<ChessFigure> allPawns, ChessFigure pawn)
        {
            var col = pawn.Col;
            var row = pawn.Row;
            var result = false;
            foreach (ChessFigure p in allPawns)
            {
                if (p.Col == col && p.Row > row)
                    result = true;
            }
            return result;
        }
        public static bool IsPawnDiagonals(List<ChessFigure> allPawns, ChessFigure pawn)
        {
            var col = pawn.Col;
            var row = pawn.Row;
            var result = false;
            foreach (ChessFigure p in allPawns)
            {
                if (p.Col + 1 == col || p.Col - 1 == col && p.Row + 1 > row)
                    result = true;
            }
            return result;
        }

        public static double GetMateCoeff(string fen)
        {
            ChessGame game = new ChessGame(fen);
            var player = game.WhoseTurn;
            var board = game.GetBoard();
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    var piece = board[i][j];
                    if (piece != null)
                    {
                        if (IsPieceKing(player, piece))
                        {
                            var file = fileMapping[j];
                            var rank = rankMapping[7 - i];
                            var position = new Position { File = file, Rank = rank };
                            var validMoves = game.GetValidMoves(position);
                            if (validMoves != null)
                            {
                                var validMoveCount = validMoves.Count();
                                return 1 - (double)validMoveCount / 8.0;
                            }
                        }

                    }

                }

            }

            return 0;
        }

        public static double GetDirectMateCoeff(string fen)
        {
            ChessGame game = new ChessGame(fen);
            var player = game.WhoseTurn;
            var board = game.GetBoard();
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    var piece = board[i][j];
                    if (piece != null)
                    {
                        if (IsPieceKing(player, piece))
                        {
                            var file = fileMapping[j];
                            var rank = rankMapping[7 - i];
                            var position = new Position { File = file, Rank = rank };
                            var validMoves = game.GetValidMoves(position);
                            if (validMoves != null)
                            {
                                var validMoveCount = validMoves.Count();
                                var coeff = validMoveCount == 0 ? 0.0 : ((double)validMoveCount - 1.0) / 7.0;
                                return coeff;
                            }
                        }

                    }

                }

            }

            return 0;
        }
        public static bool IsPieceKing(ChessDotNet.Player player, Piece piece)
        {
            return piece.GetType() == typeof(King) && piece.Owner == player;
        }
    }
}
