﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Main
{
    public class ChessGameDTO
    {
        public int Id { get; set; }
        public string WhitePlayer { get; set; }
        public string BlackPlayer { get; set; }
        public string Date { get; set; }
        public string Result { get; set; }
        public string ECO { get; set; }
    }
}
