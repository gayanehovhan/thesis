﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Main
{
    public static class FenRepository
    {

        private static Dictionary<int, List<string>> fenCache = new Dictionary<int, List<string>>();
        private static string connectionString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=Chess";
        public static IEnumerable<string> GetFenByGameIdAndMove(int gameId, int moveId)
        {
            var fens = new List<string>();
            if (fenCache.ContainsKey(gameId))
            {
                fens = fenCache[gameId];
            }
            else
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    var command = new SqlCommand("getFenByMove", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    var p1 = new SqlParameter("@gameId", System.Data.SqlDbType.Int);
                    var p2 = new SqlParameter("@moveNumber", System.Data.SqlDbType.Int);
                    p1.Value = gameId;
                    p2.Value = moveId;
                    command.Parameters.Add(p1);
                    command.Parameters.Add(p2);
                    connection.Open();
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var fen = reader.GetString(0);
                        fens.Add(fen);
                    }
                    fenCache.Add(gameId, fens);
                }
            }
            return fens;
        }

        public static IEnumerable<ChessGameDTO> GetGamesByFilter(string playerName, string color, string playerDate, string oponentName)
        {
            var games = new List<ChessGameDTO>();
            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand("getGameByFilter", connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                var p1 = new SqlParameter("@playerName", System.Data.SqlDbType.NVarChar);
                var p2 = new SqlParameter("@color", System.Data.SqlDbType.NVarChar);
                var p3 = new SqlParameter("@playerDate", System.Data.SqlDbType.NVarChar);
                var p4 = new SqlParameter("@oponentName", System.Data.SqlDbType.NVarChar);
                p1.Value = playerName == "null" ? (object)DBNull.Value : playerName;
                p2.Value = color == "null" ? (object)DBNull.Value : color;
                p3.Value = playerDate == "0" ? (object)DBNull.Value : playerDate;
                p4.Value = oponentName == "null" ? (object)DBNull.Value : oponentName;
                command.Parameters.Add(p1);
                command.Parameters.Add(p2);
                command.Parameters.Add(p3);
                command.Parameters.Add(p4);
                connection.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var game = new ChessGameDTO
                    {
                        Id = reader.GetInt32(0),
                        WhitePlayer = reader.GetString(1),
                        BlackPlayer = reader.GetString(2),
                        Date = reader.GetString(3),
                        Result = reader.GetString(4),
                        ECO=reader.GetString(5)
                    };
                    games.Add(game);
                }                
            }
            return games;
        }


        public static IEnumerable<Player> GetPlayers()
        {
            var players = new List<Player>();
            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand("getAllPlayers", connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                connection.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var player = new Player
                    {
                        Id = reader.GetInt32(0),
                        Name = reader.GetString(1),

                    };
                    players.Add(player);
                }
            }
            return players;
        }

        public static IEnumerable<ChessGameDTO> GetGames()
        {
            var games = new List<ChessGameDTO>();
            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand("getAllGames", connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                connection.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var game = new ChessGameDTO
                    {
                        Id = reader.GetInt32(0),
                        WhitePlayer = reader.GetString(1),
                        BlackPlayer = reader.GetString(2),
                        Date = reader.GetString(3),
                        Result = reader.GetString(4)
                    };
                    games.Add(game);
                }
            }
            return games;
        }
        public static int GetMovesCount(int gameId)
        {
            int count = 0;
            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand("getFenCount", connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                var p1 = new SqlParameter("@gameId", System.Data.SqlDbType.Int);
                p1.Value = gameId;
                command.Parameters.Add(p1);
                connection.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    count = reader.GetInt32(0);
                }
            }
            return count;
        }
    }
}
