﻿using ChessDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Main
{
    public struct ChessFigure
    {
        public Piece figure;
        public int Row;
        public int Col;
        public int ValidMovesCount;
        public bool IsWhite
        {
            get
            {
                return figure.IsWhite();
            }
        }
    }
}
