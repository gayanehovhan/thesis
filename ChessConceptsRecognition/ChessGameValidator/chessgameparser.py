import chess
import chess.pgn
import chess.svg
import pypyodbc
import sys
import chessgamedb


try:
   dbinstance = chessgamedb.DB()
   pgn = open("Data/Spassky.pgn")
   i = 1
   while i<=2231:
        i+=1
        first_game = chess.pgn.read_game(pgn)
        white = first_game.headers["White"]
        black = first_game.headers["Black"]
        date = first_game.headers["Date"]
        result=first_game.headers["Result"]
        eco=first_game.headers["ECO"]
        gameid = dbinstance.SaveGameAndGetId(white,black,date,result,eco)
        board = first_game.board()
        for move in first_game.main_line():
                    board.push(move)                                      
                    fen = board.fen()  
                    dbinstance.SaveGameDetail(gameid,fen)
                    print(fen)
                               
except(FileNotFoundError,ValueError,AttributeError):
      print("Game is not valid!")
finally:   
    dbinstance.CloseConnection()
    print("Connection is closed!")
   


        