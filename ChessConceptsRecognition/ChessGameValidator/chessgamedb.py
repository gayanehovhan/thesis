import pypyodbc
class DB:
	connection = ''
	cursor = any
	def __init__(self):
		self.connection = pypyodbc.connect('Driver={SQL Server Native Client 11.0};'
							               'Server=localhost;'
						                   'Database=Chess;'
							               'Trusted_connection=yes')
		self.cursor = self.connection.cursor()

	def SaveGameAndGetId(self,white,black,date,result,eco):
		SQLCommand = ("INSERT INTO Game "
									  "(White, Black,Date,Result,ECO) "
									   "VALUES (?,?,?,?,?)")
		Values = [white,black,date,result,eco] 
		self.cursor.execute(SQLCommand,Values) 
		self.connection.commit() 
		gameid = self.cursor.execute('SELECT max(Id) as ID FROM  Game')
		for row in gameid:      
		  return row[0]

	def SaveGameDetail(self,gameid,fen):
		 SQLCommand = ("INSERT INTO GameDetail"
									  "(GameId, Fen) "
									   "VALUES (?,?)")
		 Values = [gameid,fen] 
		 self.cursor.execute(SQLCommand,Values) 
		 self.connection.commit() 

	def CloseConnection(self):
		self.connection.close() 


