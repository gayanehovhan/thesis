﻿using Chess.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Chess.Api.Controllers
{

    [RoutePrefix("api/concepts")]
    public class ConceptsController : ApiController
    {
        [Route("games")]
        public IEnumerable<ChessGameDTO> Get()
        {
            return FenRepository.GetGames();
        }
        [Route("gamebyfilter"),HttpGet]
        public IEnumerable<ChessGameDTO> GetGamesByFilter(string playerName,string color,string playerDate,string oponentName)
        {
            return FenRepository.GetGamesByFilter(playerName, color, playerDate, oponentName);
        }
        [Route("players"),HttpGet]
        public IEnumerable<Player> GetPlayers()
        {
            return FenRepository.GetPlayers();
        }
        [Route("move/{gameId}/{moveId}")]
        public ChessConceptDTO Get(int gameId, int moveId)
        {
            var fens = FenRepository.GetFenByGameIdAndMove(gameId, moveId).ToList();
            var fen = string.Empty;
            if (moveId < fens.Count())
            {
                fen = fens[moveId];
                var concept = ChessConcepts.GetConcepts(fen);
                return concept;
            }
            else
            {
                return ChessConceptDTO.Empty;
            }

        }
        [Route("move/byfen"), HttpGet]
        public ChessConceptDTO GetConceptsByFen(string fen)
        {
            return ChessConcepts.GetConcepts(fen);
        }
        [Route("move/movescount/{gameId}"), HttpGet]
        public int GetMovesCount(int gameId)
        {
            return FenRepository.GetMovesCount(gameId);
        }
        [Route("move/mateonking"), HttpGet]
        public double GetMateCoefficent(string fen)
        {
            return ChessConcepts.GetMateCoeff(fen);
        }
        [Route("move/directmateonking"), HttpGet]
        public double GetDirectMateCoefficent(string  fen)
        {
            return ChessConcepts.GetDirectMateCoeff(fen);
        }
    }
}